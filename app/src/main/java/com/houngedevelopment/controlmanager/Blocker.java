package com.houngedevelopment.controlmanager;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;
import android.widget.ToggleButton;

public class Blocker extends Activity implements
        OnCheckedChangeListener {
    static final String TAG = "DevicePolicyActivity";
    static final int ACTIVATION_REQUEST = 47;
    DevicePolicyManager devicePolicyManager;
    ComponentName DeviceAdmin;
    ToggleButton toggleButton;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocker);

        toggleButton = (ToggleButton) super
                .findViewById(R.id.toggle_device_admin);
        toggleButton.setOnCheckedChangeListener(this);


        devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        DeviceAdmin = new ComponentName(this, AdminReceiver.class);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_lock_device:
                // We lock the screen
                Toast.makeText(this, "Locking Calling...", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Locking Calling now");
                devicePolicyManager.lockNow();
                break;
            case R.id.button_reset_device:
                // We reset the device - this will erase entire /data partition!
                Toast.makeText(this, "Locking Texting...", Toast.LENGTH_LONG).show();
                Log.d(TAG, "Locking texting now");
                devicePolicyManager.wipeData(ACTIVATION_REQUEST);
                break;
        }
    }


    @Override
    public void onCheckedChanged(CompoundButton button, boolean isChecked) {
        if (isChecked) {
            // Activate device administration
            Intent intent = new Intent(
                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                    DeviceAdmin);
            intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
                    "Control Manager Needs Permissions");
            startActivityForResult(intent, ACTIVATION_REQUEST);
        }
        Log.d(TAG, "onCheckedChanged to: " + isChecked);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ACTIVATION_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    Log.i(TAG, "Administration enabled!");
                    toggleButton.setChecked(true);
                } else {
                    Log.i(TAG, "Administration enable FAILED!");
                    toggleButton.setChecked(false);
                }
                return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}